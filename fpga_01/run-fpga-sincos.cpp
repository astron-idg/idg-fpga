#include <complex>

#include "common/common.h"

int main(int argc, char **argv)
{
    if (argc > 2) {
	    std::cerr << "usage: " << argv[0] << " file.aocx" << std::endl;
	    exit(1);
    }

    // Initialize OpenCL
    cl::Context context;
    std::vector<cl::Device> devices;
    init(context, devices);
    cl::Device &device = devices[0];

    // Setup parameters
    const int n = 32;

    // Initialize data structures on host
    std::clog << ">>> Initialize data structures on host" << std::endl;
    float input[n];
    std::complex<float> output_1[n];
    std::complex<float> output_2[n];

    for (unsigned i = 0; i < n; i++) {
        input[i] = i - 0.1 * 3.14 + 0.5 * i;
    }

    // Allocate data structures on device
    std::clog << ">>> Allocate data structures on device" << std::endl;
    auto sizeof_input  = n * sizeof(float);
    auto sizeof_output = n * sizeof(std::complex<float>);
    cl::Buffer d_input    = cl::Buffer(context, CL_MEM_READ_ONLY, sizeof_input);
    cl::Buffer d_output_1 = cl::Buffer(context, CL_MEM_READ_WRITE, sizeof_output);
    cl::Buffer d_output_2 = cl::Buffer(context, CL_MEM_READ_WRITE, sizeof_output);
    std::clog << std::endl;

    // Create command queue
    cl::CommandQueue queue(context, device, CL_QUEUE_PROFILING_ENABLE);

    // Copy data structures to device
    std::clog << ">>> Copy data structures to device" << std::endl;
    queue.enqueueWriteBuffer(d_input, CL_TRUE, 0, sizeof_input, input);

    // Get program
    std::string filename_bin = std::string(argc == 2 ? argv[1] : "sincos.aocx");
    cl::Program program = get_program(context, device, filename_bin);

    // Setup FPGA kernels
    cl::Kernel inputKernel(program, "data_reader");
    inputKernel.setArg(0, d_input);
    inputKernel.setArg(1, n);

    cl::Kernel outputKernel(program, "data_writer");
    outputKernel.setArg(0, d_output_1);
    outputKernel.setArg(1, d_output_2);
    outputKernel.setArg(2, n);

    // Setup one queue per kernel
    std::vector<cl::CommandQueue> queues(2);
    for (cl::CommandQueue &queue : queues) {
        queue = cl::CommandQueue(context, device, CL_QUEUE_PROFILING_ENABLE);
    }

    // Run FPGA kernels
    std::clog << ">>> Run fpga" << std::endl;
    try {
        queues[0].enqueueTask(inputKernel);
        queues[1].enqueueTask(outputKernel);
    } catch (cl::Error &error) {
        std::cerr << "Error launching kernel: " << error.what() << std::endl;
        exit(EXIT_FAILURE);
    }
    queues[1].finish();
    std::clog << std::endl;

    // Copy data to host
    queue.enqueueReadBuffer(d_output_1, CL_TRUE, 0, sizeof_output, output_1);
    queue.enqueueReadBuffer(d_output_2, CL_TRUE, 0, sizeof_output, output_2);

    // Print data
    for (unsigned i = 0; i < n; i++) {
        std::complex<float> o1 = output_1[i];
        std::complex<float> o2 = output_2[i];
        std::complex<float> diff = o1 - o2;
        std::cout << std::scientific;
        std::cout << "[" << i << "] " << o1 << " - " << o2 << " = " << diff << std::endl;
    }

    return EXIT_SUCCESS;
}
