#pragma OPENCL EXTENSION cl_intel_channels : enable

#include <ihc_apint.h>

#define NR_POLARIZATIONS            4
#define SUBGRID_SIZE                32
#define NR_PIXELS                   ((SUBGRID_SIZE) * (SUBGRID_SIZE))

#include "fft.h"

typedef float2  Subgrids[][NR_POLARIZATIONS][NR_PIXELS];

channel float2 input_pixel_channel, ffted_pixel_channel;


/*
    Subgrid reader
*/
__attribute__((max_global_work_dim(0)))
__kernel void subgrid_reader(__global const volatile Subgrids subgrids_in, unsigned nr_subgrids)
{
    #pragma loop_coalesce 3
    for (unsigned subgrid = 0; subgrid < nr_subgrids; subgrid++) {
        for (unsigned pol = 0; pol < NR_POLARIZATIONS; pol++) {
            for (unsigned pixel = 0; pixel < NR_PIXELS; pixel++) {
                write_channel_intel(input_pixel_channel, subgrids_in[subgrid][pol][pixel]);
            }
        }
    }
}


/*
    FFT kernel
*/
__kernel
__attribute__((autorun))
__attribute__((max_global_work_dim(0)))
void fft_kernel()
{
    float2 data[32][32] __attribute__((doublepump));

    #pragma unroll 1
    for (unsigned int pol = 0; pol < NR_POLARIZATIONS; pol++) {

        #pragma loop_coalesce 2
        for (unsigned short y = 0; y < SUBGRID_SIZE; y++) {
            for (unsigned short x = 0; x < SUBGRID_SIZE; x++) {
                data[y][x] = read_channel_intel(input_pixel_channel);
            }
        }

        fft_32x32(data, FFT_BACKWARD);

        #pragma loop_coalesce 2
        for (unsigned short y = 0; y < SUBGRID_SIZE; y++) {
            for (unsigned short x = 0; x < SUBGRID_SIZE; x++) {
	                write_channel_intel(ffted_pixel_channel, data[bit_reverse_32(y)][bit_reverse_32(x)]);
            }
        }
    }
}


/*
    Subgrid writer
*/
__attribute__((max_global_work_dim(0)))
__kernel void subgrid_writer(__global Subgrids subgrids_out, unsigned nr_subgrids)
{
    #pragma loop_coalesce 3
    for (unsigned subgrid = 0; subgrid < nr_subgrids; subgrid++) {
        for (unsigned pol = 0; pol < NR_POLARIZATIONS; pol++) {
            for (unsigned pixel = 0; pixel < NR_PIXELS; pixel++) {
                subgrids_out[subgrid][pol][pixel] = read_channel_intel(ffted_pixel_channel);
            }
        }
    }
}
