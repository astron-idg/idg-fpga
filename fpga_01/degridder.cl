#pragma OPENCL EXTENSION cl_intel_channels : enable

#include <ihc_apint.h>

#if !defined NR_GRIDDERS
#define NR_GRIDDERS		    20
#endif

//#define USE_SIN_COS_LOOKUP_TABLE

#define SUBGRID_SIZE                32
#define NR_TIMESTEPS                128
#define NR_CHANNELS                 16
#define UNROLL_FACTOR               4U
#define NR_STATIONS                 64
#define NR_POLARIZATIONS            4

#define NR_PIXELS                   ((SUBGRID_SIZE) * (SUBGRID_SIZE))
#define NR_VISIBILITIES             ((NR_TIMESTEPS) * (NR_CHANNELS))

#define REAL                        0
#define IMAG                        1
#define COMPLEX                     2

#include "cosisin.h"
#include "fft.h"
#include "math.h"

#if NR_CHANNELS < 4 || NR_CHANNELS % 4 != 0
#error FIXME: phase_index_channel should send four phase indices
#endif


typedef float8  Visibilities[][NR_VISIBILITIES];
typedef float   UVWOffsets[][3];
typedef float   UVW[][NR_TIMESTEPS][3];
typedef float   LMN[NR_PIXELS][3];
typedef float   WaveNumbers[NR_CHANNELS];
typedef float8  Aterms[NR_STATIONS][NR_PIXELS];
typedef uint2   StationPairs[];
typedef float   Spheroidal[NR_PIXELS];
typedef float2  Subgrids[][NR_POLARIZATIONS][NR_PIXELS];


channel float8  visibilities_channel_1[NR_GRIDDERS], visibilities_channel_2 __attribute__((depth(NR_VISIBILITIES)));
channel float   uvw_channel_1[NR_GRIDDERS], uvw_channel_2[NR_GRIDDERS];
channel float   lmn_channel[NR_GRIDDERS] __attribute__((depth(NR_PIXELS * 3)));
channel float   phase_offset_channel[NR_GRIDDERS] __attribute__((depth(NR_PIXELS)));
channel float   phase_index_channel[NR_GRIDDERS];
channel float4  phases_channel[NR_GRIDDERS];
channel float8  phasors_channel[NR_GRIDDERS];
channel float   wave_number_channel_1[NR_GRIDDERS], wave_number_channel_2[NR_GRIDDERS];
channel float8  pixel_channel[NR_GRIDDERS] __attribute__((depth(NR_PIXELS)));
channel float8  combined_pixel_channel, aterm_corrected_pixel_channel;
channel float2  spheroidal_corrected_channel, ffted_pixel_channel, input_pixel_channel;


/*
   UVW reader
*/
__attribute__((max_global_work_dim(0)))
__kernel void uvw_reader(__global const volatile UVW uvw, unsigned nr_subgrids)
{
    #pragma loop_coalesce
    for (unsigned subgrid = 0; subgrid < nr_subgrids; subgrid += NR_GRIDDERS) {
        for (unsigned gridder = 0; gridder < NR_GRIDDERS; gridder++) {
            for (unsigned time = 0; time < NR_TIMESTEPS; time++) {
                for (unsigned index = 0; index < 3; index++) {
                    float tmp = subgrid + gridder < nr_subgrids ? uvw[subgrid + gridder][time][index] : 0.0f;
                    write_channel_intel(uvw_channel_1[gridder], tmp);
                }
            }
        }
    }
}


/*
    UVW repeater
*/
__attribute__((max_global_work_dim(0)))
__attribute__((autorun))
__attribute__((num_compute_units(NR_GRIDDERS)))
__kernel void uvw_repeater()
{
    int gridder = get_compute_id(0);

    while (true) {
        float uvw[NR_TIMESTEPS][3] __attribute__((max_concurrency(2)));

        #pragma loop_coalesce
        for (unsigned short time = 0; time < NR_TIMESTEPS; time++) {
            for (unsigned short index = 0; index < 3; index++) {
                uvw[time][index] = read_channel_intel(uvw_channel_1[gridder]);
            }
        }

        #pragma loop_coalesce
        for (unsigned pixel_major = 0; pixel_major < NR_PIXELS; pixel_major += UNROLL_FACTOR) {
            for (unsigned time = 0; time < NR_TIMESTEPS; time++) {
                for (unsigned pixel_minor = 0; pixel_minor < UNROLL_FACTOR; pixel_minor++) {
                    for (unsigned index = 0; index < 3; index++) {
                        write_channel_intel(uvw_channel_2[gridder], uvw[time][index]);
                    }
                }
            }
        }
    }
}


/*
    LMN reader
*/
__attribute__((max_global_work_dim(0)))
__kernel void lmn_reader(__global const volatile LMN lmn, unsigned nr_subgrids)
{
    #pragma loop_coalesce
    for (unsigned subgrid_major = 0; subgrid_major < nr_subgrids; subgrid_major += NR_GRIDDERS) {
        for (unsigned pixel = 0; pixel < NR_PIXELS; pixel++) {
            for (unsigned index = 0; index < 3; index++) {
                float _lmn = lmn[pixel][index];

                #pragma unroll
                for (unsigned gridder = 0; gridder < NR_GRIDDERS; gridder++) {
                    write_channel_intel(lmn_channel[gridder], _lmn);
                }
            }
        }
    }
}


/*
    Phase index
*/
__attribute__((max_global_work_dim(0)))
__attribute__((autorun))
__attribute__((num_compute_units(NR_GRIDDERS)))
__kernel void phase_index()
{
    int gridder = get_compute_id(0);

    while (true) {
        float _lmn[UNROLL_FACTOR][3] __attribute__((max_concurrency(2)));

        #pragma loop_coalesce
        for (unsigned short pixel_minor = 0; pixel_minor < UNROLL_FACTOR; pixel_minor++) {
            for (uint2_t index = 0; index < 3; index++) {
                _lmn[pixel_minor][index] = read_channel_intel(lmn_channel[gridder]);
            }
        }

        #pragma loop_coalesce 2
        for (unsigned short time = 0; time < NR_TIMESTEPS; time++) {
            for (unsigned short pixel_minor = 0; pixel_minor < UNROLL_FACTOR; pixel_minor++) {
                float phase_index = 0;

                for (uint2_t index = 0; index < 3; index++) {
                    phase_index += _lmn[pixel_minor][index] * read_channel_intel(uvw_channel_2[gridder]);
                }

                write_channel_intel(phase_index_channel[gridder], phase_index);
            }
        }
    }
}


/*
    UVW offsets reader
*/
__attribute__((max_global_work_dim(0)))
__kernel void uvw_offsets_reader(__global const volatile UVWOffsets *restrict uvw_offsets, __global const volatile LMN *restrict lmn, unsigned nr_subgrids)
{
    #pragma loop_coalesce 3
    for (unsigned subgrid = 0; subgrid < nr_subgrids;) {
        for (unsigned gridder = 0; gridder < NR_GRIDDERS; gridder ++, subgrid++) {
            for (unsigned pixel = 0; pixel < NR_PIXELS; pixel++) {
                float phase_offset = 0;

                #pragma unroll 1
                for (uint2_t index = 0; index < 3; index++) {
                    float uvw_offset = subgrid < nr_subgrids ? (*uvw_offsets)[subgrid][index] : 0.0f;
                    float lmn_value = (*lmn)[pixel][index];
                    phase_offset += uvw_offset * lmn_value;
                }

                write_channel_intel(phase_offset_channel[gridder], phase_offset);
            }
        }
    }
}


/*
    Wavenumber reader
*/
__attribute__((max_global_work_dim(0)))
__kernel void wave_numbers_reader(__global const volatile WaveNumbers wave_numbers, unsigned nr_subgrids)
{
    WaveNumbers _wave_numbers;

    #pragma unroll 1
    for (unsigned chan = 0; chan < NR_CHANNELS; chan++) {
        _wave_numbers[chan] = wave_numbers[chan];
    }

    #pragma loop_coalesce
    for (unsigned subgrid_major = 0; subgrid_major < nr_subgrids; subgrid_major += NR_GRIDDERS) {
        for (unsigned gridder = 0; gridder < NR_GRIDDERS; gridder++) {
            for (unsigned chan = 0; chan < NR_CHANNELS; chan++) {
                write_channel_intel(wave_number_channel_1[gridder], _wave_numbers[chan]);
            }
        }
    }
}


/*
    Wavenumber repeater
*/
__attribute__((max_global_work_dim(0)))
__attribute__((autorun))
__attribute__((num_compute_units(NR_GRIDDERS)))
__kernel void wave_number_repeater()
{
    int gridder = get_compute_id(0);

    while (true) {
        WaveNumbers _wave_numbers __attribute__((max_concurrency(2)));;

        for (unsigned short chan = 0; chan < NR_CHANNELS; chan++) {
            _wave_numbers[chan] = read_channel_intel(wave_number_channel_1[gridder]);
        }

        #pragma loop_coalesce
        for (unsigned pix_major = 0; pix_major < NR_PIXELS; pix_major += UNROLL_FACTOR) {
            for (unsigned time = 0; time < NR_TIMESTEPS; time++) {
                for (unsigned chan = 0; chan < NR_CHANNELS; chan++) {
                    write_channel_intel(wave_number_channel_2[gridder], _wave_numbers[chan]);
                }
            }
        }
    }
}


/*
    Visibilities writer
*/
__attribute__((max_global_work_dim(0)))
#if defined EMULATOR
__kernel void visibilities_writer(__global Visibilities visibilities, __global const unsigned *nr_subgrids_ptr)
#else
__kernel void visibilities_writer(__global Visibilities visibilities, unsigned nr_subgrids)
#endif
{
#if defined EMULATOR
    unsigned nr_subgrids = *nr_subgrids_ptr;
#endif

    #pragma loop_coalesce
    for (unsigned subgrid = 0; subgrid < nr_subgrids;) {
        for (unsigned gridder = 0; gridder < NR_GRIDDERS; gridder ++, subgrid++) {
            for (unsigned visibility = 0; visibility < NR_VISIBILITIES; visibility++) {
                float8 visibility_value = read_channel_intel(visibilities_channel_2);

                if (subgrid < nr_subgrids) {
                    visibilities[subgrid][visibility] = visibility_value;
                }
            }
        }
    }
}


/*
    Visibilities multiplexer
*/
__attribute__((max_global_work_dim(0)))
__attribute__((autorun))
__kernel void visibilities_multiplexer()
{
    #pragma loop_coalesce
    for (unsigned gridder = 0; gridder < NR_GRIDDERS; gridder++) {
        for (unsigned visibility = 0; visibility < NR_VISIBILITIES; visibility++) {
            write_channel_intel(visibilities_channel_2, read_channel_intel(visibilities_channel_1[gridder]));
        }
    }
}


/*
    Phases
*/
__attribute__((max_global_work_dim(0)))
__attribute__((autorun))
__attribute__((num_compute_units(NR_GRIDDERS)))
__kernel void phases()
{
    int gridder = get_compute_id(0);

    while (true) {
        float4 phase_offsets;

        for (unsigned short pix_minor = 0; pix_minor < UNROLL_FACTOR; pix_minor++) {
            phase_offsets[pix_minor] = read_channel_intel(phase_offset_channel[gridder]);
        }

        for (unsigned short time = 0; time < NR_TIMESTEPS; time++) {
            float4 phase_indices;

            for (unsigned short pix_minor = 0; pix_minor < UNROLL_FACTOR; pix_minor++) {
                phase_indices[pix_minor] = read_channel_intel(phase_index_channel[gridder]);
            }

            for (unsigned short chan = 0; chan < NR_CHANNELS; chan++) {
                float  wave_number = read_channel_intel(wave_number_channel_2[gridder]);
                float4 phases      = (phase_indices * wave_number) - phase_offsets;
                write_channel_intel(phases_channel[gridder], phases);
            }
        }
    }
}


/*
    Phasors
*/
__attribute__((max_global_work_dim(0)))
__attribute__((autorun))
__attribute__((num_compute_units(NR_GRIDDERS)))
__kernel void phasors()
{
    int gridder = get_compute_id(0);

    while (true) {
        float4 phases = read_channel_intel(phases_channel[gridder]);
        float2 phasors[UNROLL_FACTOR];

        #pragma unroll
        for (unsigned i = 0; i < UNROLL_FACTOR; i++) {
            phasors[i] = cosisin(phases[i]);
        }

        write_channel_intel(phasors_channel[gridder], (float8) (phasors[0], phasors[1], phasors[2], phasors[3]));
    }
}


/*
    Degridder
*/
__attribute__((max_global_work_dim(0)))
__attribute__((autorun))
__attribute__((num_compute_units(NR_GRIDDERS)))
__kernel void degridder()
{
    int degridder = get_compute_id(0);
    float8 visibilities[NR_VISIBILITIES];

    for (unsigned short visibility = 0; visibility < NR_VISIBILITIES; visibility++) {
        visibilities[visibility] = 0;
    }

    #pragma ivdep
    for (unsigned short pix_major = 0; pix_major < NR_PIXELS; pix_major += UNROLL_FACTOR) {
        float8 pixels[UNROLL_FACTOR] __attribute__((register));

        for (unsigned short pix_minor = 0; pix_minor < UNROLL_FACTOR; pix_minor++) {
            pixels[pix_minor] = read_channel_intel(pixel_channel[degridder]);
        }

        for (unsigned short visibility = 0; visibility < NR_VISIBILITIES; visibility++) {
            float8 visibility_value = visibilities[visibility];
            float8 phasors          = read_channel_intel(phasors_channel[degridder]);

            #pragma unroll
            for (unsigned short pix_minor = 0; pix_minor < UNROLL_FACTOR; pix_minor++) {
                visibility_value.even += phasors[pix_minor * COMPLEX + REAL] * pixels[pix_minor].even + -phasors[pix_minor * COMPLEX + IMAG] * pixels[pix_minor].odd;
                visibility_value.odd  += phasors[pix_minor * COMPLEX + REAL] * pixels[pix_minor].odd  +  phasors[pix_minor * COMPLEX + IMAG] * pixels[pix_minor].even;
            }

            visibilities[visibility] = visibility_value;
        }
    }

    for (unsigned short visibility = 0; visibility < NR_VISIBILITIES; visibility++) {
        write_channel_intel(visibilities_channel_1[degridder], visibilities[visibility]);
    }
}


/*
    Pixel channel multiplexer
*/
__attribute__((max_global_work_dim(0)))
__attribute__((autorun))
__kernel void pixel_channel_multiplexer()
{
    #pragma loop_coalesce
    for (unsigned gridder = 0; gridder < NR_GRIDDERS; gridder++) {
        for (unsigned pixel = 0; pixel < NR_PIXELS; pixel++) {
            write_channel_intel(pixel_channel[gridder], read_channel_intel(aterm_corrected_pixel_channel));
        }
    }
}


/*
    Aterm
*/
__attribute__((max_global_work_dim(0)))
__kernel void aterm(
    __global const volatile Aterms *restrict aterms,
    __global const volatile StationPairs *restrict stationPairs,
    unsigned nr_subgrids)
{
    #pragma loop_coalesce 2
    for (unsigned subgrid = 0; subgrid < nr_subgrids;) {
        for (unsigned gridder = 0; gridder < NR_GRIDDERS; gridder ++, subgrid++) {
            uint2 stationPair;

            if (subgrid < nr_subgrids) {
                stationPair = (*stationPairs)[subgrid];
            }

            for (unsigned short pixel = 0; pixel < NR_PIXELS; pixel++) {
                float8 aterm[2], pixel_value = subgrid < nr_subgrids ? read_channel_intel(combined_pixel_channel) : (float8) 0;

                #pragma unroll 1
                for (uint2_t s = 0; s < 2; s++) {
                    aterm[s] = (*aterms)[stationPair[s]][pixel];
                }

                pixel_value = matmul(hermitian(aterm[1]), matmul(pixel_value, aterm[0]));
                write_channel_intel(aterm_corrected_pixel_channel, pixel_value);
            }
        }
    }
}


/*
    Pixel polarization joiner
*/
__attribute__((max_global_work_dim(0)))
__attribute__((autorun))
__kernel void pixel_polarization_joiner()
{
    union {
        float8 f8;
        float2 f2[4];
    } pixels[NR_PIXELS];

    #pragma loop_coalesce
    for (unsigned short pol = 0; pol < NR_POLARIZATIONS; pol++) {
        for (unsigned short pixel = 0; pixel < NR_PIXELS; pixel++) {
            pixels[pixel].f2[pol] = read_channel_intel(spheroidal_corrected_channel);
        }
    }

    for (unsigned short pixel = 0; pixel < NR_PIXELS; pixel++) {
        write_channel_intel(combined_pixel_channel, pixels[pixel].f8);
    }
}


/*
    Spheroidal
*/
__attribute__((max_global_work_dim(0)))
__kernel void spheroidal(__global const volatile Spheroidal spheroidal, unsigned nr_subgrids)
{
    Spheroidal _spheroidal;

    for (unsigned short pixel = 0; pixel < NR_PIXELS; pixel++) {
        _spheroidal[pixel] = spheroidal[pixel];
    }

    #pragma loop_coalesce
    for (unsigned count = 0; count < nr_subgrids * NR_POLARIZATIONS; count++) {
        for (unsigned pixel = 0; pixel < NR_PIXELS; pixel++) {
            write_channel_intel(spheroidal_corrected_channel, _spheroidal[pixel] * read_channel_intel(ffted_pixel_channel));
        }
    }
}


/*
    FFT kernel
*/
__kernel
__attribute__((autorun))
__attribute__((max_global_work_dim(0)))
void fft_kernel()
{
    float2 data[32][32] __attribute__((doublepump, max_concurrency(2)));

    #pragma loop_coalesce 2
    for (unsigned short y = 0; y < SUBGRID_SIZE; y++) {
	for (unsigned short x = 0; x < SUBGRID_SIZE; x++) {
	    data[y][x] = read_channel_intel(input_pixel_channel);
	}
    }

    fft_32x32(data, FFT_FORWARD);

    #pragma loop_coalesce 2
    for (unsigned short y = 0; y < SUBGRID_SIZE; y++) {
	for (unsigned short x = 0; x < SUBGRID_SIZE; x++) {
	    write_channel_intel(ffted_pixel_channel, data[bit_reverse_32(y)][bit_reverse_32(x)]);
	}
    }
}


/*
    Subgrid reader
*/
__attribute__((max_global_work_dim(0)))
#if defined EMULATOR
__kernel void subgrid_reader(__global const volatile Subgrids subgrids, __global const unsigned *nr_subgrids_ptr)
#else
__kernel void subgrid_reader(__global const volatile Subgrids subgrids, unsigned nr_subgrids)
#endif
{
#if defined EMULATOR
    unsigned nr_subgrids = *nr_subgrids_ptr;
#endif

    #pragma loop_coalesce 3
    for (unsigned subgrid = 0; subgrid < nr_subgrids; subgrid++) {
        for (unsigned pol = 0; pol < NR_POLARIZATIONS; pol++) {
            for (unsigned pixel = 0; pixel < NR_PIXELS; pixel++) {
                write_channel_intel(input_pixel_channel, subgrids[subgrid][pol][pixel]);
            }
        }
    }
}
