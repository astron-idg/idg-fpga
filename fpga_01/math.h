#if !defined MATH_H
#define MATH_H

inline float2 cmul(float2 a, float2 b)
{
    return (float2) (a.x * b.x + -a.y * b.y, a.x * b.y + a.y * b.x);
}


inline float8 matmul(float8 matrixA, float8 matrixB)
{
    float2 a[2][2] = {{ matrixA.s01, matrixA.s23 }, { matrixA.s45, matrixA.s67 }};
    float2 b[2][2] = {{ matrixB.s01, matrixB.s23 }, { matrixB.s45, matrixB.s67 }};
    float2 c[2][2];

    #pragma unroll 1
    for (uint2_t y = 0; y < 2; y++) {
        #pragma unroll
        for (uint2_t x = 0; x < 2; x++) {
            c[y][x] = cmul(a[0][x], b[y][0]) + cmul(a[1][x], b[y][1]);
        }
    }

    return (float8) (c[0][0], c[0][1], c[1][0], c[1][1]);
}


inline float8 conjugate(float8 matrix)
{
    matrix.odd = -matrix.odd;
    return matrix;
}


inline float8 transpose(float8 matrix)
{
    return matrix.s01452367;
}


inline float8 hermitian(float8 matrix)
{
    return transpose(conjugate(matrix));
}

#endif
