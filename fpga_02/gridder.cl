#pragma OPENCL EXTENSION cl_intel_channels : enable

#include <ihc_apint.h>

//#define USE_SIN_COS_LOOKUP_TABLE

#define SUBGRID_SIZE                32
#define NR_TIMESTEPS                128
#define NR_CHANNELS                 16
#define UNROLL_CHANNELS             8
#define UNROLL_PIXELS               4
#define NR_STATIONS                 64
#define NR_POLARIZATIONS            4

#define NR_PIXELS                   ((SUBGRID_SIZE) * (SUBGRID_SIZE))
#define NR_VISIBILITIES             ((NR_TIMESTEPS) * (NR_CHANNELS))

#define REAL                        0
#define IMAG                        1
#define COMPLEX                     2

#include "cosisin.h"

typedef float8  Visibilities[][NR_TIMESTEPS][NR_CHANNELS];
typedef float   UVWOffsets[][4];
typedef float   WaveNumbers[NR_CHANNELS];
typedef float   UVW[][NR_TIMESTEPS][4];
typedef float   LMN[NR_PIXELS][4];
typedef float2  Subgrids[][NR_POLARIZATIONS][NR_PIXELS];


/*
   Gridder
*/
__attribute__((max_global_work_dim(0)))
__attribute__((uses_global_work_offset(0)))
__kernel void gridder(
        __global const WaveNumbers *restrict wavenumbers,
        __global const LMN *restrict lmn,
        __global const volatile UVW *restrict uvw,
        __global const volatile UVWOffsets *restrict uvw_offsets,
        __global const volatile Visibilities *restrict visibilities,
        __global volatile Subgrids *restrict subgrids,
        unsigned nr_subgrids)
{
    float l[NR_PIXELS];
    float m[NR_PIXELS];
    float n[NR_PIXELS];

    float wavenumbers_[NR_CHANNELS];

    for (unsigned short pixel = 0; pixel < NR_PIXELS; pixel++) {
        l[pixel] = (*lmn)[pixel][0];
        m[pixel] = (*lmn)[pixel][1];
        n[pixel] = (*lmn)[pixel][2];
    }

    for (unsigned short chan = 0; chan < NR_CHANNELS; chan++) {
        wavenumbers_[chan] = (*wavenumbers)[chan];
    }

    float8 subgrid[NR_PIXELS] __attribute__((singlepump));
    float8 phases[UNROLL_PIXELS] __attribute__((register));

    #pragma ivdep
    for (unsigned int subgrid_major = 0; subgrid_major < nr_subgrids; subgrid_major++) {

        float u_offset = (*uvw_offsets)[subgrid_major][0];
        float v_offset = (*uvw_offsets)[subgrid_major][1];
        float w_offset = (*uvw_offsets)[subgrid_major][2];

        #pragma ivdep
        for (unsigned int i = 0; i < NR_TIMESTEPS * NR_CHANNELS; i += UNROLL_CHANNELS) {
            unsigned short time = i / NR_CHANNELS;
            unsigned short chan_major = i % NR_CHANNELS;

            float8 scales;
            #pragma unroll
            for (unsigned short chan_minor = 0; chan_minor < UNROLL_CHANNELS; chan_minor++) {
                scales[chan_minor] = wavenumbers_[chan_major + chan_minor];
            }

            float u = (*uvw)[subgrid_major][time][0];
            float v = (*uvw)[subgrid_major][time][1];
            float w = (*uvw)[subgrid_major][time][2];

            for (unsigned short pixel_major = 0; pixel_major < NR_PIXELS; pixel_major += UNROLL_PIXELS) {

                float8 visibilities_[NR_CHANNELS] __attribute__((singlepump));
                for (unsigned short chan_minor = 0; chan_minor < UNROLL_CHANNELS; chan_minor++) {
                    unsigned short chan = chan_major + chan_minor;
                    visibilities_[chan] = (*visibilities)[subgrid_major][time][chan];
                }

                #pragma unroll
                for (unsigned short pixel_minor = 0; pixel_minor < UNROLL_PIXELS; pixel_minor++) {
                    unsigned short pixel = pixel_major + pixel_minor;
                    float8 pixel_value  = (i == 0) ? (float8) (0.0f) : subgrid[pixel];
                    float phase_indices = l[pixel] * u + m[pixel] * v + n[pixel] * w;
                    float phase_offsets = l[pixel] * u_offset + m[pixel] * v_offset + n[pixel] * w_offset;
                    phases[pixel_minor] = -phase_indices * scales + phase_offsets;

                    #pragma unroll
                    for (unsigned short chan_minor = 0; chan_minor < UNROLL_CHANNELS; chan_minor++) {
                        float8 visibility = visibilities_[chan_major + chan_minor];
                        float2 phasor = cosisin(phases[pixel_minor][chan_minor]);
                        pixel_value.even += phasor.x * visibility.even + -phasor.y * visibility.odd;
                        pixel_value.odd  += phasor.x * visibility.odd  +  phasor.y * visibility.even;
                    }

                    subgrid[pixel] = pixel_value;
                } // end for pixel_minor
            } // end for pixel_major
        } // end for i

        #pragma ivdep
        for (unsigned short pixel = 0; pixel < NR_PIXELS; pixel++) {
            (*subgrids)[subgrid_major][0][pixel] = subgrid[pixel].s01;
            (*subgrids)[subgrid_major][1][pixel] = subgrid[pixel].s23;
            (*subgrids)[subgrid_major][2][pixel] = subgrid[pixel].s45;
            (*subgrids)[subgrid_major][3][pixel] = subgrid[pixel].s67;
        }
    } // end for subgrid_major
}
